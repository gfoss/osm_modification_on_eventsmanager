<?php

$toPath = "/wp-content/plugins/events-manager/" ; 

global $EM_Event;
$required = apply_filters('em_required_html','<i>*</i>');
?>
<?php if( !get_option('dbem_require_location') && !get_option('dbem_use_select_for_locations') ): ?>
<div class="em-location-data-nolocation">
	<p>
		<input type="checkbox" name="no_location" id="no-location" value="1" <?php if( $EM_Event->location_id === '0' || $EM_Event->location_id === 0 ) echo 'checked="checked"'; ?> />
		<?php _e('This event does not have a physical location.','events-manager'); ?>
	</p>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$('#no-location').change(function(){
				if( $('#no-location').is(':checked') ){
					$('#em-location-data').hide();
				}else{
					$('#em-location-data').show();
				}
			}).trigger('change');
		});
	</script>
</div>
<?php endif; ?>

<div id="em-location-data" class="em-location-data">
	<div id="location_coordinates" style='display: none;'>
		<input id='location-latitude' name='location_latitude' type='text' value='<?php echo esc_attr($EM_Event->get_location()->location_latitude); ?>' size='15' />
		<input id='location-longitude' name='location_longitude' type='text' value='<?php echo esc_attr($EM_Event->get_location()->location_longitude); ?>' size='15' />
	</div>
	<?php if( get_option('dbem_use_select_for_locations') || !$EM_Event->can_manage('edit_locations','edit_others_locations') ) : ?> 
	<table class="em-location-data">
		<tr class="em-location-data-select">
			<th><?php esc_html_e('Location:','events-manager') ?> </th>
			<td> 
				<select name="location_id" id='location-select-id' size="1">  
					<?php if(!get_option('dbem_require_location',true)): ?><option value="0"><?php esc_html_e('No Location','events-manager'); ?></option><?php endif; ?>
					<?php
					$ddm_args = array('private'=>$EM_Event->can_manage('read_private_locations'));
					$ddm_args['owner'] = (is_user_logged_in() && !current_user_can('read_others_locations')) ? get_current_user_id() : false;
					$locations = EM_Locations::get($ddm_args);
					$selected_location = !empty($EM_Event->location_id) || !empty($EM_Event->event_id) ? $EM_Event->location_id:get_option('dbem_default_location');
					foreach($locations as $EM_Location) {
						$selected = ($selected_location == $EM_Location->location_id) ? "selected='selected' " : '';
						if( $selected ) $found_location = true;
				   		?>          
				    	<option value="<?php echo esc_attr($EM_Location->location_id) ?>" title="<?php echo esc_attr("{$EM_Location->location_latitude},{$EM_Location->location_longitude}"); ?>" <?php echo $selected ?>><?php echo esc_html($EM_Location->location_name); ?></option>
				    	<?php
					}
					if( empty($found_location) && !empty($EM_Event->location_id) ){
						$EM_Location = $EM_Event->get_location();
						if( $EM_Location->post_id ){
							?>
					    	<option value="<?php echo esc_attr($EM_Location->location_id) ?>" title="<?php echo esc_attr("{$EM_Location->location_latitude},{$EM_Location->location_longitude}"); ?>" selected="selected"><?php echo esc_html($EM_Location->location_name); ?></option>
					    	<?php
						}
					}
					?>
				</select>
			</td>
		</tr>
	</table>
	<?php else : ?>
	
	
	
	
	
	
<script language="JavaScript" type="text/javascript">
function checon() { 
  gax = 0 ; 
  var errors ;
  var query ;
if(document.getElementById('location-address').value == '' ) { 
  errors = ' Παρακαλώ συμπληρώστε την Διεύθυνση.  \r\n' ; 
  gax = 1 ; }

if(document.getElementById('location-town').value == '' ) { 
  errors = errors + ' Παρακαλώ συμπληρώστε την Πόλη/Χωριό. \r\n' ;
  gax = 1 ; }

if(document.getElementById('location-country').value == '0' ) { 
  errors = errors + ' Παρακαλώ επιλέξτε την Χώρα.  \r\n' ; 
  gax = 1 ; }

if(gax == 1) { 
	 alert( errors ) ;
	      return false ; 
   } else {
	
	
	query = document.getElementById('location-address').value + ', ' + document.getElementById('location-town').value + ', ' + document.getElementById('location-country').value; 
	
	    console.log( "Έτοιμος!: " + query ) ;
	
	
	genmap(query);
	
	
	     console.log( "Τέλος!: " + query ) ;
		
		    
	    return true  ;
	 }
}
</script>


	
	
	
	
	
	<table class="em-location-data">
		<?php 
			global $EM_Location;
			if( $EM_Event->location_id !== 0 ){
				$EM_Location = $EM_Event->get_location();
			}elseif(get_option('dbem_default_location') > 0){
				$EM_Location = em_get_location(get_option('dbem_default_location'));
			}else{
				$EM_Location = new EM_Location();
			}
		?>
		<tr class="em-location-data-name">
			<th><?php _e ( 'Location Name:', 'events-manager')?></th>
			<td>
				<input id='location-id' name='location_id' type='hidden' value='<?php echo esc_attr($EM_Location->location_id); ?>' size='15' />
				<input id="location-name" type="text" name="location_name" value="<?php echo esc_attr($EM_Location->location_name, ENT_QUOTES); ?>" /><?php echo $required; ?>													
				<br />
				<em id="em-location-search-tip"><?php esc_html_e( 'Create a location or start typing to search a previously created location.', 'events-manager')?></em>
				<em id="em-location-reset" style="display:none;"><?php esc_html_e('You cannot edit saved locations here.', 'events-manager'); ?> <a href="#"><?php esc_html_e('Reset this form to create a location or search again.', 'events-manager')?></a></em>
			</td>
 		</tr>
		<tr class="em-location-data-address">
			<th><?php _e ( 'Address:', 'events-manager')?>&nbsp;</th>
			<td>
				 <input id="location-address" type="text" name="location_address" value="<?php echo esc_attr($EM_Location->location_address); ; ?>" /><?php echo $required; ?></td>
		</tr>
		<tr class="em-location-data-town">
			<th><?php _e ( 'City/Town:', 'events-manager')?>&nbsp;</th>
			<td>
				<input id="location-town" type="text" name="location_town" value="<?php echo esc_attr($EM_Location->location_town); ?>"  onfocusout="checon();"  /><?php echo $required; ?>
			</td>
		</tr>
		<tr class="em-location-data-state">
			<th><?php _e ( 'State/County:', 'events-manager')?>&nbsp;</th>
			<td>
				<input id="location-state" type="text" name="location_state" value="<?php echo esc_attr($EM_Location->location_state); ?>" />
			</td>
		</tr>
		<tr class="em-location-data-postcode">
			<th><?php _e ( 'Postcode:', 'events-manager')?>&nbsp;</th>
			<td>
				<input id="location-postcode" type="text" name="location_postcode" value="<?php echo esc_attr($EM_Location->location_postcode); ?>" />
			</td>
		</tr>
		<tr class="em-location-data-region">
			<th><?php _e ( 'Region:', 'events-manager')?>&nbsp;</th>
			<td>
				<input id="location-region" type="text" name="location_region" value="<?php echo esc_attr($EM_Location->location_region); ?>" />
			</td>
		</tr>
		<tr class="em-location-data-country">
			<th><?php _e ( 'Country:', 'events-manager')?>&nbsp;</th>
			<td>
				<select id="location-country" name="location_country">
					<option value="0" <?php echo ( $EM_Location->location_country == '' && $EM_Location->location_id == '' && get_option('dbem_location_default_country') == '' ) ? 'selected="selected"':''; ?>><?php _e('none selected','events-manager'); ?></option>
					<?php foreach(em_get_countries() as $country_key => $country_name): ?>
					<option value="<?php echo esc_attr($country_key); ?>" <?php echo ( $EM_Location->location_country == $country_key || ($EM_Location->location_country == '' && $EM_Location->location_id == '' && get_option('dbem_location_default_country')==$country_key) ) ? 'selected="selected"':''; ?>><?php echo esc_html($country_name); ?></option>
					<?php endforeach; ?>
				</select><?php echo $required; ?>
			</td>
		</tr>
	</table>
	
	
	<div id="osmbox">
	
   
      <script type="text/javascript" src="<?php echo $toPath; ?>leaflet.js"></script>
      <link rel="stylesheet" type="text/css" href="<?php echo $toPath; ?>leaflet.css">
      <script type="text/javascript" src="<?php echo $toPath; ?>bundle.min.js"></script>
      
	  	<!--- location.php  ----> 
  <style id="compiled-css" type="text/css">
      #map { width: 600px; height: 400px; float: left; }
      #map img { max-height: none; }
  </style>
  	
  <div id="map"></div>

<hr /> 

 <a href="javascript: document.getElementById('publish').classList.add('disabled'); 
		              document.getElementById('post-preview').classList.add('disabled');
		              document.getElementById('save-post').classList.add('disabled');">Disable</a> | 
 <a href="javascript: document.getElementById('publish').classList.remove('disabled'); 
		              document.getElementById('post-preview').classList.remove('disabled');
		              document.getElementById('save-post').classList.remove('disabled'); 
	                   document.getElementById('location-latitude').value = 0;
	                   document.getElementById('location-longitude').value = 0;">Μανωλιός Enable!</a>

  <script>
	  
	  
	  
	  
	  
	// Initialize map to specified coordinates
    var map = L.map('map', {
      center: [21.5, -0.1], // CAREFULL!!! The first position corresponds to the lat (y) and the second to the lon (x)
      zoom: 15
    });

    // Add tiles (streets, etc)
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      subdomains: ['a', 'b', 'c']
    }).addTo(map);
	
	
	markerArray = [];
	
    markerGroup = L.featureGroup(markerArray) ;
		
function genmap(addr) {
	
		
	markerGroup.clearLayers();
	markerArray = [];
	markerAddress = [];
	console.log(markerArray);
	console.log(markerAddress);

//n/    var query_addr = "99 Southwark St, London SE1 0JF, UK";
  //  var query_addr = "Ακαδημίας 100";
    var query_addr = addr;

    // Get the provider, in this case the OpenStreetMap (OSM) provider. For some reason, this is the "wrong" way to instanciate it. Instead, we should be using an import "leaflet-geosearch" but I coulnd't make that work
    const provider = new window.GeoSearch.OpenStreetMapProvider({
          params: {
            addressdetails: 1
          },
    });
    var query_promise = provider.search({
      query: query_addr
    });


var SelectedIcon = L.icon({
    iconUrl: "<?php echo $toPath; ?>images/marker-icon_org.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [0, -30]
});

// the marker that we will use to reset the icon to the other marks ;)
var JustIcon = L.icon({
    iconUrl: "<?php echo $toPath; ?>images/marker-icon.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [0, -30]
});


    markerGroup = L.featureGroup(markerArray) ;
	markerGroup.on("click", function (e) {
		var clickedMarker = e.layer;
		
		console.debug(clickedMarker);
		   
		markerGroup.eachLayer(function (layer) {
			layer.setIcon(JustIcon) ;
		});
		
		clickedMarker.setIcon(SelectedIcon);
		
            document.getElementById('location-latitude').value = clickedMarker.getLatLng().lat;
            document.getElementById('location-longitude').value = clickedMarker.getLatLng().lng;
			
		document.getElementById('publish').classList.remove('disabled');
		document.getElementById('post-preview').classList.remove('disabled');
		document.getElementById('save-post').classList.remove('disabled');
		 
	});
	markerGroup.addTo(map);
	
	var vallen=-1;
	
     query_promise.then(value => {
		vallen = value.length;
		console.debug("DEBUGING PROMISE RESPONSE:" + vallen + " V.L" + value.length);
		if(vallen == 1){
			var label = value[0].label;
			var marker = new L.marker([value[0].y, value[0].x]).addTo(markerGroup) // CAREFULL!!! The first position corresponds to the lat (y) and the second to the lon (x)
			
			marker.bindPopup("<b>Found location</b><br>" + label).openPopup(); // note the "openPopup()" method. It only works on the marker
			marker.setIcon(SelectedIcon);
			markerArray.push(marker);
		}
		else{
		  for (i = 0; i < vallen; i++) {
			// Success!
			var x_coor = value[i].x;
			var y_coor = value[i].y;
			
			console.log("CORDS: x: "+ x_coor+"  y: "+y_coor + " i:" + i) ;
			console.debug(value[i]);
			var label = value[i].label;
			var marker = new L.marker([y_coor, x_coor]).addTo(markerGroup) // CAREFULL!!! The first position corresponds to the lat (y) and the second to the lon (x)
			
			marker.bindPopup("<b>Found location</b><br>" + label).openPopup(); // note the "openPopup()" method. It only works on the marker
			
			markerArray.push(marker);
		  };
	  }
      
      console.debug("After For: " + vallen + " V.L: " + value.length);
      if( vallen > 0) 
		map.fitBounds(markerGroup.getBounds());
      
    console.log(vallen + " pinezes");
	 document.getElementById('location-latitude').value = 0;
	 document.getElementById('location-longitude').value = 0;
	if( vallen > 1) {
	    document.getElementById('publish').classList.add('disabled') ;
		document.getElementById('post-preview').classList.add('disabled');
		document.getElementById('save-post').classList.add('disabled');
	    alert(" Παρακαλώ επιλέξτε πινέζα (marker) για να ολοκληρωθεί η δημοσίευση. \n Σε αντίθετη περίπτωση δεν θα είναι δυνατή η Δημοσίευση.") 
	 } else { 
		 if(vallen == 1) { 
		
            document.getElementById('location-latitude').value = markerArray[0].getLatLng().lat;
            document.getElementById('location-longitude').value = markerArray[0].getLatLng().lng;
			 
		 }
		document.getElementById('publish').classList.remove('disabled');
		document.getElementById('post-preview').classList.remove('disabled');
		document.getElementById('save-post').classList.remove('disabled');
		 
	 }
    
    }, reason => { 
      console.log(reason); // Error!
    });

}

  </script>
	
	
	
</div><!----- END OF DIV OSMBOX ----->
	
	
	
	
	
	
	
	<?php endif; ?>
	<?php if ( get_option( 'dbem_gmap_is_active' ) ) em_locate_template('forms/map-container.php',true); ?>
	<br style="clear:both;" />
</div>