	<!--- locationmap.php  ----> 
<?php 

$toPath = "/wp-content/plugins/events-manager/" ; 

/*
 * This file contains the HTML generated for a single location Google map. You can copy this file to yourthemefolder/plugins/events/templates and modify it in an upgrade-safe manner.
 * 
 * There is one argument passed to you, which is the $args variable. This contains the arguments you could pass into shortcodes, template tags or functions like EM_Events::get().
 * 
 * In this template, we encode the $args array into JSON for javascript to easily parse and request the locations from the server via AJAX.
 */
	/* @var $EM_Location EM_Location */
	
	
	
/* 2019_08_08 konp, julosame to : "get_option('dbem_gmap_is_active') &&"  apo thn arxh tou if kaqos den qa xrhsimopoihsoume google maps.
*                  Epishs to metatrepoume se OpenStreetMaps! "
*/

// DEBUG USAGE! 
//	echo "EM: " . is_object($EM_Location) . "\nLAT: " . $EM_Location->location_latitude . "\nLON: " . $EM_Location->location_longitude . "\n";
	if ( ( is_object($EM_Location) && $EM_Location->location_latitude != 0 && $EM_Location->location_longitude != 0 ) ) {
?>

	<style type="text/css">
      #map {  height: 400px; width: 600px; }
      #map img { max-height: none; }
	</style>
   
      <script type="text/javascript" src="<?php echo $toPath; ?>leaflet.js"></script>
      <link rel="stylesheet" type="text/css" href="<?php echo $toPath; ?>leaflet.css">
      <script type="text/javascript" src="<?php echo $toPath; ?>bundle.min.js"></script>
	
  <div id="map"></div>


  <script>
	  
	// Initialize map to specified coordinates

    var map = L.map('map', {
      center: [<?php echo $EM_Location->location_latitude; ?>, <?php echo $EM_Location->location_longitude; ?>], // CAREFULL!!! The first position corresponds to the lat (y) and the second to the lon (x)
      zoom: 15
    });

    // Add tiles (streets, etc)
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      subdomains: ['a', 'b', 'c']
    }).addTo(map);
	
	
		
function genmap(addr) {
	
    var query_addr = addr;

    const provider = new window.GeoSearch.OpenStreetMapProvider();
    var query_promise = provider.search({
      query: query_addr
    });


var JustIcon = L.icon({
    iconUrl: "<?php echo $toPath; ?>images/marker-icon.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [0, 30]
});

    query_promise.then(value => {
	//	console.debug("DEBUGING PROMISE RESPONSE");
      if (value.length > 0) {
        // Success!
        var x_coor = value[0].x;
        var y_coor = value[0].y;
        
     //   console.log("CORDS: x: "+ x_coor+"  y: "+y_coor) ;
	 //   console.debug(value[0]);
        var label = value[0].label;
        var marker = new L.marker([y_coor, x_coor]).addTo(map) // CAREFULL!!! The first position corresponds to the lat (y) and the second to the lon (x)
        
        marker.bindPopup("<b>Found location</b><br>" + label).openPopup(); // note the "openPopup()" method. It only works on the marker
        marker.setIcon(JustIcon) ;
        
      };
      
    }, reason => {
      console.log(reason); // Error!
    });

}

  genmap("<?php echo $EM_Location->location_latitude; ?>, <?php echo $EM_Location->location_longitude; ?>") ;

  </script>	

<?php
    }	
	
?>
