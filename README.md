# osm_modification_on_eventsmanager

Based on EventsManager (version 5.9.5) plugin for WordPress (https://wordpress.org/plugins/events-manager/) by Marcus Sykes  (http://wp-events-plugin.com/), we removed google maps dependencies and developed an OpenStreetMap (OSM) integration instead, using Leaflet.js

* When we add an event or location,for the Address to Coordinates request we use OpenStreetMap API
* We use OSM and Leaflet.js to create a map centered on the result pin of the search
* The guest of the site will see the location on OSM embeded map

Except the above, we also added some functionality that fit our needs, to select specific point when the returned results are more than one. Also we didn't want the admin to be able to move the pointer, and to select only one, by disabling Save button. Finaly, we wanted administrator to be able to remove the pointer.

The usage of OpenStreetMap API (Nominatim) offers limited capacity. Please read https://wiki.openstreetmap.org/wiki/Nominatim for more details.
Our needs are 5 - 10 requests per week, so we are fine and much lower than their limits!

Be aware that if you saved locations using googlemaps, there might be minor differences on the location of the pin that appears on OpenStreetMap (mostly different street number).

# Changes 

The following files have been changed to fullfil our needs:

**templates/forms/event/location.php**

It shows the form in wp-admin at the Event creation. When the user types first the address and then the city, the JavaScript function checon() runs, and checks if the fields address, city and country are not empty. If any of them are empty it shows an alert that prompts the user to fill them. If they are not empty, it creates the query string and it passes it to the JavaScript function genmap(). Finaly, it disables the buttons for Save/ Preview/ and Publish.

Above genmap() lies the code that initialises the map. At first the map is centered on the location returned by the API, which is marked by a blue pin. If there are more than one results, it zooms in/out and centers to fit all of them. Inside GenMap we specify the action when the admin selects a marker unselects (if any) previous selected marker, passes the coordinates in the hidden fields (location-latitude / location-longitude), makes it orange, and enables the previously disabled buttons.

**templates/forms/location/where.php** 

It shows the form at the location input or edit. Same functionality as above.

**templates/placeholders/locationmap.php** 

Shows the map on the site, at event's page.
